package education.boo.education_7th.utils;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by jins on 2016-04-07.
 */
public class CommonUtil {

    /**
     * @param context
     * @return Device Width, Height
     */
    public static int[] getDeviceSize(Context context) {
        int[] size = new int[2];

        int MeasuredWidth;
        int MeasuredHeight;
        Point point = new Point();
        WindowManager wm = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            wm.getDefaultDisplay().getSize(point);
            MeasuredWidth = point.x;
            MeasuredHeight = point.y;
        } else {
            Display d = wm.getDefaultDisplay();
            MeasuredWidth = d.getWidth();
            MeasuredHeight = d.getHeight();
        }

        if (MeasuredHeight > MeasuredWidth) {
            size[0] = MeasuredWidth;
            size[1] = MeasuredHeight;
        } else {
            size[0] = MeasuredHeight;
            size[1] = MeasuredWidth;
        }

        return size;
    }

    public static int getDeviceWidth(Context context) {
        int[] size = getDeviceSize(context);
        return size[0];
    }

    public static int getDeviceHeight(Context context) {
        int[] size = getDeviceSize(context);
        return size[1];
    }
}
