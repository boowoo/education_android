package education.boo.education_7th.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import education.boo.education_7th.R;
import education.boo.education_7th.utils.CommonUtil;

/**
 * Created by jins on 2016-04-24.
 */
public class CustomYesOrNoDialog extends Dialog {

    private RelativeLayout dialogBody;
    private TextView cancelText;
    private TextView confirmText;

    private String dialogTitle;

    private OnYesOrNoDialogClickListener onYesOrNoDialogClickListener;

    public interface OnYesOrNoDialogClickListener {
        void onConfirmClick();
    }

    public void setOnDialogClick(OnYesOrNoDialogClickListener listener){
        this.onYesOrNoDialogClickListener = listener;
    }

    public CustomYesOrNoDialog(Context context, String dialogTitle) {
        super(context);
        this.dialogTitle = dialogTitle;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;

        getWindow().setAttributes(lpWindow);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        setContentView(R.layout.yes_or_no_custom_popup);

        dialogBody = (RelativeLayout) findViewById(R.id.dialog_body);
        ((TextView) findViewById(R.id.ask_text)).setText(dialogTitle);

        cancelText = (TextView) findViewById(R.id.cancel_btn);

        confirmText = (TextView) findViewById(R.id.confirm_btn);
        confirmText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onYesOrNoDialogClickListener != null){
                    onYesOrNoDialogClickListener.onConfirmClick();
                }
                CustomYesOrNoDialog.this.dismiss();
            }
        });

        setBodyWidth();
    }

    private void setBodyWidth(){
        ViewGroup.MarginLayoutParams lparam = (ViewGroup.MarginLayoutParams) dialogBody.getLayoutParams();

        lparam.width = CommonUtil.getDeviceWidth(getContext()) - lparam.leftMargin;
        dialogBody.setLayoutParams(lparam);
    }
}
