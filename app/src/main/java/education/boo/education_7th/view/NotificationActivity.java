package education.boo.education_7th.view;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;

import education.boo.education_7th.R;

public class NotificationActivity extends Activity {

	private int notificationId = 0;

	private Button button_default;
	private Button button_image;
	private Button button_text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);

		button_default = (Button) findViewById(R.id.noti_default_btn);
		button_default.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				makeDefalutNotification();
				notificationId++;
			}
		});

		button_image = (Button) findViewById(R.id.noti_large_img_btn);
		button_image.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				makeImageNotification();
				notificationId++;
			}
		});

		button_text = (Button) findViewById(R.id.noti_large_text_btn);
		button_text.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				makeTextNotification();
				notificationId++;
			}
		});
	}



	private void makeDefalutNotification(){
		Intent intent = new Intent();
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationManager notiMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this).
				setContentTitle("Default Push").
				setContentText("Content =============== Content").
				setSmallIcon(R.mipmap.ic_launcher).
				setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.abc_ic_menu_copy_mtrl_am_alpha)).
				setAutoCancel(true).
				setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE).setPriority(NotificationCompat.PRIORITY_MAX);
		builder.setContentIntent(contentIntent);
		notiMgr.notify(notificationId, builder.build());
	}

	private void makeImageNotification(){
		Intent intent = new Intent();
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationManager notiMgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder compatBuilder = new NotificationCompat.Builder(this)
				.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
				.setPriority(NotificationCompat.PRIORITY_MAX)
				.setContentTitle("Large Push")
				.setContentText("Content =============== Content")
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentIntent(contentIntent)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

		NotificationCompat.BigPictureStyle bigStyle = new NotificationCompat.BigPictureStyle(compatBuilder)
				.bigPicture(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
				.setBigContentTitle("Large Big Content Title")
				.setSummaryText("Large Big Content ================ Content");

		compatBuilder.setStyle(bigStyle);

		Notification noti = compatBuilder.build();

		noti.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS;
		noti.defaults |= (Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
		noti.ledARGB = Color.GREEN;
		noti.ledOnMS = 500;
		noti.ledOffMS = 500;
		notiMgr.notify(notificationId, noti);
	}

	private void makeTextNotification(){
		Intent intent = new Intent();
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationManager notiMgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

		NotificationCompat.Builder compatBuilder = new NotificationCompat.Builder(this)
				.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
				.setPriority(NotificationCompat.PRIORITY_MAX)
				.setContentTitle("Large Push")
				.setContentText("Content =============== Content")
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentIntent(contentIntent)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

		new NotificationCompat.BigTextStyle(compatBuilder)
				.bigText("long ~ long ~ very very long ~ long ~ long ~ very very long ~ long ~ long ~ very very long ~ long ~ long ~ very very long ~ ")
				.setBigContentTitle("Large Big Content Title")
				.setSummaryText("Large Big Content ================ Content");

		Notification noti = compatBuilder.build();

		noti.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS;
		noti.defaults |= (Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
		noti.ledARGB = Color.GREEN;
		noti.ledOnMS = 500;
		noti.ledOffMS = 500;
		notiMgr.notify(notificationId, noti);
	}
}
