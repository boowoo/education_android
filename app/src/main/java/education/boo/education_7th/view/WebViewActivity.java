package education.boo.education_7th.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import education.boo.education_7th.R;

public class WebViewActivity extends Activity {

	private WebView webView;
	private ProgressBar progressbar;
	private final int LOADING_COMPLETE = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);

		progressbar = (ProgressBar) findViewById(R.id.progress_bar);

		webView = (WebView) findViewById(R.id.web_view);
		webView.loadUrl("http://www.coupang.com");

		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

		webView.addJavascriptInterface(new JSInterface(), "MYJSTest");
		webView.loadUrl("javascript:MYJSTest.hello()");

		webView.setWebViewClient(new WebClient());
		webView.setWebChromeClient(new WebViewChrome());
	}

	class JSInterface {
		@JavascriptInterface
		public void hello() {
			Toast.makeText(WebViewActivity.this, "Hello javaScript!!", Toast.LENGTH_SHORT).show();
		}
	}


	class WebClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.contains("plp")) {
				view.loadUrl("http://www.google.com");
				return false;
			}

			view.loadUrl(url);
			return true;
		}
	}

	class WebViewChrome extends WebChromeClient {

		@Override
		public void onProgressChanged(WebView view, int newProgress) {

			if(newProgress == LOADING_COMPLETE){
				progressbar.setVisibility(View.GONE);
			}else{
				progressbar.setVisibility(View.VISIBLE);
			}
		}
	}
}
