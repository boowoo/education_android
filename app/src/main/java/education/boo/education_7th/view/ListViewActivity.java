package education.boo.education_7th.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import education.boo.education_7th.R;

public class ListViewActivity extends Activity {

	private ListView listView;
	private BaseAdapter baseAdapter;
	private ArrayList<String> arrayData;

	private LayoutInflater mInflater;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_view);

		mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		makeListView();
	}

	private void makeListView(){
		listView = (ListView) findViewById(R.id.list_view);
		listView.setAdapter(makeAdapter());
		makeData(500);
	}

	private BaseAdapter makeAdapter(){
		baseAdapter = new UserBaseAdpter();
		return baseAdapter;
	}

	public class UserBaseAdpter extends BaseAdapter {

		@Override
		public int getCount() {
			return arrayData == null ? 0 : arrayData.size();
		}

		@Override
		public Object getItem(int position) {
			return arrayData == null ? null : arrayData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder viewHolder;

			if(convertView == null){
				convertView = mInflater.inflate(R.layout.activity_listview_row, parent, false);

				viewHolder = new ViewHolder();
				viewHolder.textView = (TextView) convertView.findViewById(R.id.text_view);

				convertView.setTag(viewHolder);
			}else{
				viewHolder = (ViewHolder) convertView.getTag();
			}

			viewHolder.textView.setText(getItem(position).toString());

			return convertView;
		}
	}

	public class ViewHolder {
		TextView textView;
		ImageView imageView;
	}

	private void makeData(int cnt){
		arrayData = new ArrayList<>();

		for(int i=0; i<cnt; i++){
			arrayData.add(i + getResources().getString(R.string.list_ordering));
		}

		baseAdapter.notifyDataSetChanged();
	}
}
