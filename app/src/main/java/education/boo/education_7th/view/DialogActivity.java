package education.boo.education_7th.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import education.boo.education_7th.R;
import education.boo.education_7th.widget.CustomYesOrNoDialog;

public class DialogActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dialog);

		Button dialogBtn = (Button) findViewById(R.id.dialog_btn);
		dialogBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog();
			}
		});
	}

	private void showDialog(){
		CustomYesOrNoDialog customYesOrNoDialog = new CustomYesOrNoDialog(this, "DialogTest");
		customYesOrNoDialog.show();
	}
}
