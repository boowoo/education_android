package education.boo.education_7th.view;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import education.boo.education_7th.R;

public class LinearListViewActivity extends Activity {

	private LinearLayout linearLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_linear_list_view);

		linearLayout = (LinearLayout) findViewById(R.id.linear_body);
		makeListView(500);
	}

	private void makeListView(int cnt){
		for(int i=0; i<cnt; i++){
			linearLayout.addView(makeTextView(i));
		}
	}

	private TextView makeTextView(int order){
		TextView textView = new TextView(this);
		textView.setText(order + getResources().getString(R.string.list_ordering));

		return  textView;
	}

	private ImageView makeImageView(){
		ImageView imageView = new ImageView(this);
		imageView.setImageResource(R.drawable.abc_ic_menu_selectall_mtrl_alpha);

		return  imageView;
	}
}
