package education.boo.education_7th.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import education.boo.education_7th.R;

public class ViewPagerActivity extends Activity implements View.OnClickListener {

	private ViewPager viewPager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_pager);

		setLayout();

		viewPager = (ViewPager)findViewById(R.id.view_pager);
		viewPager.setAdapter(new PagerAdapterClass(this));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_one:
				setCurrentInflateItem(0);
				break;
			case R.id.btn_two:
				setCurrentInflateItem(1);
				break;
			case R.id.btn_three:
				setCurrentInflateItem(2);
				break;
		}
	}

	private void setCurrentInflateItem(int type){
		if(type==0){
			viewPager.setCurrentItem(0);
		}else if(type==1){
			viewPager.setCurrentItem(1);
		}else{
			viewPager.setCurrentItem(2);
		}
	}

	private Button btn_one;
	private Button btn_two;
	private Button btn_three;

	/**
	 * Layout
	 */
	private void setLayout(){
		btn_one = (Button) findViewById(R.id.btn_one);
		btn_two = (Button) findViewById(R.id.btn_two);
		btn_three = (Button) findViewById(R.id.btn_three);

		btn_one.setOnClickListener(this);
		btn_two.setOnClickListener(this);
		btn_three.setOnClickListener(this);
	}

	/**
	 * PagerAdapter
	 */
	private class PagerAdapterClass extends PagerAdapter {

		private LayoutInflater mInflater;

		public PagerAdapterClass(Context c){
			super();
			mInflater = LayoutInflater.from(c);
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public Object instantiateItem(ViewGroup pager, int position) {
			View view;

			if(position==0){
				view = mInflater.inflate(R.layout.activity_listview_row, null);
			}
			else if(position==1){
				view = mInflater.inflate(R.layout.activity_linear_list_view, null);
			}else{
				view = mInflater.inflate(R.layout.activity_listview_row, null);
			}

			pager.addView(view, 0);

			return view;
		}

		@Override
		public void destroyItem(ViewGroup pager, int position, Object view) {
			pager.removeView((View)view);
		}

		@Override
		public boolean isViewFromObject(View pager, Object obj) {
			return pager == obj;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {}
		@Override
		public Parcelable saveState() { return null; }
		@Override
		public void startUpdate(View arg0) {}
		@Override
		public void finishUpdate(View arg0) {}
	}

}
